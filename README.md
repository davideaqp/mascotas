**REQUERIMIENTOS**

python 3.7 
pip 9.0.3 o  superior

INSTALACION 
** clonar la rama máster de gitlab
** crear entorno virtual
- python -m venv myvenv 

** activar el entorno:
- /myvenv/Script/activate  (windows)
- source myenv/bin/activate  (ubuntu)

** instalar dependencias (dentro de la carpeta del proyecto): 
- pip install -r requirements.txt

** Migrar modelos y crear superuser 
- python manage.py migrate 
- python manage.py createsuperuser

** Ejecutar Server
- python manage.py runserver 

**Pagina de inicio
http://127.0.0.1:8000/

**Ubicacion del proyecto:
http://127.0.0.1:8000/proyectoTAIS/resumen/

** deployado:
http://18.191.214.85:7000/proyectoTAIS/resumen/