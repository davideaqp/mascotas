from rest_framework import serializers
from .models import Prueba
from .models import Prueba2

class PruebaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Prueba
        fields = ('id', 'similarity')
class PruebaSerializer2(serializers.ModelSerializer):
    similarity = serializers.StringRelatedField(read_only=True)
    class Meta:
        model = Prueba2
        fields = ('id', 'similarity','filename')

class PruebaSerializer3(serializers.ModelSerializer):
    class Meta:
        model = Prueba2
        fields = ('id', 'similarity','filename')

class PruebaSerializer2(serializers.ModelSerializer):
    simi = PruebaSerializer(many= True,read_only = True)
    class Meta:
        model = Prueba2
        fields = ('id','filename','simi')
