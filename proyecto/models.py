from django.db import models
from summa.summarizer import summarize

# Create your models here.
class Prueba(models.Model):
    similarity = models.TextField(blank=True)
    def __str__(self):
        return self.similarity

class Prueba2(models.Model):
    similarity = models.ForeignKey(Prueba, on_delete = models.CASCADE,blank=True,related_name = 'simi')

    filename = models.TextField(null=True, blank=True,  default = 50)
