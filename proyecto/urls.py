from django.conf.urls import url
from django.urls import include, path
from rest_framework.routers import DefaultRouter
#from .views import (ProfileViewSet, ProfileStatusViewSet)
from .views import PruebaViewSet
from .views import PruebaViewSet2
from .views import PruebaViewSet3
# profile_list = ProfileViewSet.as_view({"get": "list"})
# profile_detail = ProfileViewSet.as_view({"get": "retrieve"})


router = DefaultRouter()
router.register(r"resumen", PruebaViewSet2)
router.register(r"similitud", PruebaViewSet)
router.register(r"crear", PruebaViewSet3)


urlpatterns = [
    path("", include(router.urls)),
    # url(r'^personaldni/(?P<dni>\d+)/$', BuscarDni.as_view(), name="PersonalDNI"),
    # url(r'^personalporespecialidad/$', BuscarEspecialidad.as_view(), name="PersonalDNI"),
    #url(r'^cancelar/(?P<dni>\d+)/$', cancelarCita.as_view(), name="cancelarCita"),
]
