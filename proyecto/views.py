from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from .serializers import PruebaSerializer
from .serializers import PruebaSerializer2
from .serializers import PruebaSerializer3
#from .serializers import PruebaSerializer4
from .models import Prueba
from .models import Prueba2

from django.shortcuts import render

class PruebaViewSet(ModelViewSet):
    queryset = Prueba.objects.all()
    serializer_class = PruebaSerializer
class PruebaViewSet2(ModelViewSet):
    queryset = Prueba2.objects.all()
    serializer_class = PruebaSerializer2
class PruebaViewSet3(ModelViewSet):
    queryset = Prueba2.objects.all()
    serializer_class = PruebaSerializer3
#class PruebaViewSet4(ModelViewSet):
#    queryset = Prueba2.objects.all()
#    serializer_class = PruebaSerializer4
